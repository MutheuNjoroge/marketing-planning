## Description (optional)
<!-- Here goes a brief description of what your post is about, and what are you trying to achieve with it. Also, if necessary, explain why it shouldn't go in any particular platform. -->

### Caption
<!-- The captions that accompanies your artwork for your post, where you develop your idea. Usually limited to 280 characters for Twitter and 500 for Mastodon, so look out for that if you're targeting one of these two platforms.

If you believe hashtags are a good fit for adding after the caption, add them here. A regular rule of thumb for accessibility is that, if your hashtag is composed of more than one word, you should use Pascal Casing (https://www.theserverside.com/definition/Pascal-case), so that screen readers can properly read the hashtags.

Some of the ones we usually use: #FedoraLinux #OpenSource #Linux -->


### Artwork
<!-- Here you should add an image, if your post has one, or ask for an artwork to be created if you need one. -->

---

## Posted on:
<!-- This is a checklist to track which platforms the post went live before closing the issue. Edit this if a post shouldn't go in a particular platform for any particular reason, preferably explaining why in the description section. -->

- [ ] Mastodon
- [ ] Twitter
- [ ] Instagram
- [ ] YouTube Community
- [ ] LinkedIn



<!--DO NOT EDIT BELOW THIS LINE!-->

/labels ~"social media (post)"
/assign @josephgayoso @not-a-dev-stein @ekidney @jwflory
