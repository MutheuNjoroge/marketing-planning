# Marketing Planning

This project is to track issues related to the Marketing Planning for the Fedora Project

## What we do

The short version: The Fedora Marketing Team ensures that people in Fedora can consistently explain to everyone what Fedora is, why the project can help them, and how they can help the project.

For more information, see our [docs](#).

## Questions, Issues, Challenges?

Submit an Issue in this GitLab Repository by clicking [here](https://gitlab.com/fedora/marketing/marketing-planning/-/issues/new).

